package com.deeeplabs.backplate;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.Window;
import android.view.WindowManager;

import com.kaopiz.kprogresshud.KProgressHUD;

import java.nio.charset.StandardCharsets;

public class InterfaceManager {

    private static InterfaceManager INTERFACEMANAGER = null;
    private Boolean isErrMsgShown = false;
    KProgressHUD hud;
    Boolean isLoading = false;

    public static InterfaceManager sharedInstance() {
        if (INTERFACEMANAGER == null) {
            INTERFACEMANAGER = new InterfaceManager();

        }
        return INTERFACEMANAGER;
    }

    public void showLoading(Context context, String title, String detail){
        if(isLoading){
            return;
        }
        isLoading = true;
        hud = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel(title)
                .setDetailsLabel(detail)
                .setCancellable(false)
                .setAnimationSpeed(3)
                .setDimAmount(0.5f);
        hud.show();
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

    public String getStringFromHex(String hex){
        int l = hex.length();
        byte[] data = new byte[l/2];
        for (int i = 0; i < l; i += 2) {
            data[i/2] = (byte) ((Character.digit(hex.charAt(i), 16) << 4)
                    + Character.digit(hex.charAt(i+1), 16));
        }
        return new String(data, StandardCharsets.UTF_8);
    }

    public void hideLoading(){
        if(!isLoading){
            return;
        }
        isLoading = false;
        hud.dismiss();
    }

    public void minimizeApp(Context context) {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(startMain);
    }

    //programmatic method to change the status bar. Using this won't handle if the status bar icons are white
    public void changeStatusWindowColorInActivity(Activity activity, int theColorResource){
        Window window = activity.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(theColorResource);
    }

}
